<?php

namespace SQLI\TechTestBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Yaml\Yaml;

class TechController extends Controller
{
    public function indexAction()
    {
        return $this->render('SQLITechTestBundle:Default:index.html.twig');
    }
    public function readQuestionsAction()
    {


        $configClassInstallerFile = __DIR__ . '/../Resources/public/files/symfony.yml';
        $configClassInstaller = Yaml::parse(file_get_contents($configClassInstallerFile));


        return $this->render('SQLITechTestBundle:Default:question.html.twig');
    }
}
